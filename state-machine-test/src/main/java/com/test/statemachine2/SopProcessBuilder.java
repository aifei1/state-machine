package com.test.statemachine2;

/**
 * @Description 通过建造者模式构建SopProcess对象
 * @ClassName SopProcessBuilder
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 14:50 - 星期日
 */
public class SopProcessBuilder {
    private SopProcess sopProcess;

    protected void setSopProcess(SopProcess sopProcess){
        this.sopProcess = sopProcess;
    }

    public static SopProcessBuilder getInstance(){
        SopProcessBuilder sopProcessBuilder = new SopProcessBuilder();
        sopProcessBuilder.setSopProcess(new SopProcess());
        return sopProcessBuilder;
    }

    public SopProcessBuilder from(State state){
        sopProcess.setFrom(state);
        return this;
    }

    public SopProcessBuilder to(State state){
        sopProcess.setTo(state);
        return this;
    }

    public SopProcessBuilder event(Event event){
        sopProcess.setEvent(event);
        return this;
    }

    public SopProcessBuilder handle(IStateHandle stateHandle){
        sopProcess.setStateHandle(stateHandle);
        return this;
    }

    public SopProcess build(){
        return sopProcess;
    }

}
