package com.test.statemachine2;

/**
 * @Description TODO
 * @ClassName State
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 14:25 - 星期日
 */
public enum State {
    REJECT,
    FINISH,
    UN_SUBMIT,
    LEADER_CHECK,
    HR_CHECK;
}
