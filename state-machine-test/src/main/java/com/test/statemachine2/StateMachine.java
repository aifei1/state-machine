package com.test.statemachine2;

import java.util.Arrays;
import java.util.List;

/**
 * @Description TODO
 * @ClassName StateMachine
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 16:09 - 星期日
 */
public class StateMachine extends AbstractMachine{
    @Override
    List<SopProcess> init() {
        return Arrays.asList(
                SopProcessBuilder.getInstance().
                        from(State.UN_SUBMIT).
                        event(Event.PASS).
                        to(State.LEADER_CHECK).
                        handle(new IStateHandle<String,String>() {
                                   @Override
                                   public String handle(String s) {
                                       switch (s){
                                           case "pass":
                                               System.out.println("申请提交到leader，leader同意...");
                                               break;
                                           case "reject":
                                               System.out.println("申请提交到leader，leader拒绝...");
                                               break;
                                           default:
                                               System.out.println("申请未知");
                                       }
                                       return "入参是："+s;
                                   }
                               }
                        ).build(),
                SopProcessBuilder.getInstance()
                        .from(State.LEADER_CHECK)
                        .event(Event.PASS)
                        .to(State.HR_CHECK)
                        .handle(new IStateHandle<String,String>() {
                            @Override
                            public String handle(String s) {
                                switch (s){
                                    case "pass":
                                        System.out.println("leader审批申请单，leader同意...到HR");
                                        break;
                                    case "reject":
                                        System.out.println("leader审批申请单，leader拒绝...");
                                        break;
                                    default:
                                        System.out.println("申请未知");
                                }
                                return "入参是："+s;
                            }
                        })
                        .build()

        );
    }

    public static void main(String[] args) {
        StateMachine stateMachine = new StateMachine();
        State nextState = stateMachine.getNextState(State.UN_SUBMIT, Event.PASS);
        System.out.println(nextState);
    }
}
