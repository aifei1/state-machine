package com.test.statemachine2;

import lombok.Data;

/**
 * @Description TODO
 * @ClassName SopProcess
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 14:48 - 星期日
 */
@Data
public class SopProcess {
    private State from;
    private State to;
    private Event event;
    private IStateHandle stateHandle;
}
