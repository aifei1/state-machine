package com.test.statemachine2;

/**
 * @Description TODO
 * @ClassName IStateHandle
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 14:49 - 星期日
 */
public interface IStateHandle<T,R> {
    R handle(T t);
}
