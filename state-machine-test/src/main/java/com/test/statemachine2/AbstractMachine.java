package com.test.statemachine2;

import com.google.common.collect.HashBasedTable;
import lombok.Data;

import java.util.List;
import java.util.Optional;

/**
 * @Description TODO
 * @ClassName Abstract
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 15:13 - 星期日
 */
public abstract  class AbstractMachine {

    @Data
    static class SopExec{
        private State nextState;
        private IStateHandle stateHandle;
    }

    private HashBasedTable<State,Event,SopExec> hashBasedTable = HashBasedTable.create();

    {
        //客户自己定义SopProcess
        List<SopProcess> sopProcesses = init();
        sopProcesses.forEach(sopProcess -> {
            SopExec sopExec =new SopExec();
            sopExec.setNextState(sopProcess.getTo());
            sopExec.setStateHandle(sopProcess.getStateHandle());
            hashBasedTable.put(sopProcess.getFrom(),sopProcess.getEvent(),sopExec);
        });
    }

    abstract List<SopProcess> init();

    public State getNextState(State currentState,Event event) {
        SopExec sopExec = hashBasedTable.get(currentState, event);
        Optional.ofNullable(sopExec).orElseThrow(()->new IllegalStateException("未知状态"));
        return sopExec.getNextState();
    }

    public IStateHandle getHandle(State state,Event event){
        SopExec sopExec = hashBasedTable.get(state, event);
        Optional.ofNullable(sopExec).orElseThrow(()->new IllegalStateException("未知状态"));
        return sopExec.getStateHandle();
    }


}
