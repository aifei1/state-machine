package com.test.statemachine2;

/**
 * @Description TODO
 * @ClassName Event
 * @Author bzh
 * @Version 1.0.0
 * @CreateTime 2022/10/9 14:42 - 星期日
 */
public enum Event {
    REJECT,
    PASS;
}
